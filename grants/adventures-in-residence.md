# Adventures in Residence Grants

This is a series of $5000 grants to support those on the frontier of the
playground. We want to empower those who are pioneering new adventure
structures, and embarking on ambitous projects.

These adventures and adventurers are breaking traditional notions of learning
systems and inspiring future adventurers to do the same.

## Applying For a Grant

Just put together your [adventure
document](../README.md#whats-a-learning-adventure) and [create an
issue](https://gitlab.com/fathom/playground/issues/new?template=AIR) in this
repo with the linked template.

## Evalutating Grants
Grants applications are evaluated by 3 reviewers, randomly selected from this
list:

| @djujuu      | @jaredpereira |
| @mhibino     | @alexsingh    |
| @joelamouche |               |

Reviewer who have previous connections to the adventurer are passed over.

Once the reviewers have been assigned to your application each reviewer
independantly reviews yourt adventure. They then submit their perspective, a yes
or no with a justification for it, as a comment to the issue. The justification
should include both the aspects of your adventure the reviewer valued and those
they felt could be improved.

Each reviewer then has an opportunity to revise their verdict based on the other
judges inputs. If they do they must explicitly state what point motivated the
change.

If your applicantion passes with a majority you will be contacted to
provide an Ethereum address. The grant will paid out in DAI.

If a majority of reviewers voted no, you're absolutely encouraged to iterate on
the application and apply again. A goal of this process is to generate useful
feedback and learnings.

## Past Grants:
None yet! But soon you!
